
gcloud config set project kube-tutorial-194819
gcloud config set compute/zone us-west1-a

gcloud container clusters create talent-clusters --machine-type "n1-standard-1" --num-nodes "2" --network "default"

gcloud container clusters get-credentials talent-clusters

gcloud compute disks create mongo-disk

kubectl create -f kube/db-controller.yml
kubectl create -f kube/db-service.yml
# set docker image id in web-controller.yml
kubectl create -f kube/web-controller.yml
kubectl create -f kube/web-servie.yml

kubectl get pods
kubectl get services

kubectl autoscale rs foo --min=2 --max=5 --cpu-percent=80

