import * as bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import * as express from 'express';
import { connect } from './connection';
import * as path from 'path';
import {Votes} from './Vote';

// load environment variables
dotenv.config();

// connect to mongo database
connect();

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser());


app.get('/', async(_req, res) => {
  const fileName = path.join(__dirname, '../public', 'got_talent.html');
  console.log(fileName);
  console.log(await Votes.find({}));
  res.sendFile(fileName);
});
app.post('/', async(req, res ) => {
  console.log(req.body);
  const vote = new Votes({choice: req.body.choice});
  vote.save();
  res.end('Tanii sanaliig avlaa');
});

const { PORT } = process.env;

app.listen(PORT, () => {
  console.log('Server is up and running on port number ' + PORT);
})
