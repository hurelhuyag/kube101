import { Document, model, Schema} from "mongoose";

export interface IVoteDocument extends Document{

  _id: string;
  choice: string;
}
export const voteSchema = new Schema({
  choice: {type: String, required: true}
})

// tslint:disable-next-line
const Votes = model<IVoteDocument>('votes', voteSchema);
export {
  Votes
}
